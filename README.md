# IS1101 Assignment 9

Write a C programme to complete the following tasks.

Create a file named "assignment9.txt" and open it in write mode. Write the sentence "UCSC is one of the leading institutes in Sri Lanka for computing studies." to the file.

Open the file in read mode and read the text from the file and print it to the output screen.

Open the file in append mode and append the sentence "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields." to the file.
